Ghosts = {}

function Ghosts.init()
	Ghosts.list = {}
	Ghosts.period = 0.5
	Ghosts.deathdelay = 5
	Ghosts.imgs = { 
		{
		love.graphics.newImage("img/monster1a.png"),
		love.graphics.newImage("img/monster1b.png")
		},
		{
		love.graphics.newImage("img/tentakel1.png"),
		love.graphics.newImage("img/tentakel2.png")
		},
		{
		love.graphics.newImage("img/sunflower1.png"),
		love.graphics.newImage("img/sunflower.png")
		},
		{
		love.graphics.newImage("img/eyes.png"),
		love.graphics.newImage("img/eyes1.png")
		},
	}
	for i=1,4 do
		local posx,posy
		local invalid = true
		while invalid do
			posx = math.random(table.getn(maze[1]))
			posy = math.random(table.getn(maze))
			invalid = (maze[posy][posx] ~= 0 or harmattanDistance({pac.ix,pac.iy},{posx,posy}) < 10)
		end
		table.insert(Ghosts.list, { 
			x = 40, 
			y = 200,
			ix = posx,
			iy = posy,
			ix_prev = posx,
			iy_prev = posy,
			ghostType = (i%4)+1,
			timer = Ghosts.period,
			deathtimer = 0,
			opacity = 1,
			frameNdx = 1,
			id = (i%4)+1
		})
		Ghosts.getPosition(Ghosts.list[i])
	end

	deathsound = love.audio.newSource("sound/splurge.ogg")
end

function Ghosts.update(dt)
	for i,v in ipairs(Ghosts.list) do
		Ghosts.updateGhost(dt, v)
	end
end

function Ghosts.checkdeath(ghost)
	-- check death
	if ghost.deathtimer > 0 then
		return
	end

	local xghost, yghost = ghost.ix_prev, ghost.iy_prev
	if ghost.timer < Ghosts.period/2 then
		xghost = ghost.ix
		yghost = ghost.iy
	end

	local xpac, ypac = pac.ix_prev, pac.iy_prev
	if pac.movetimer < pac.movedelay/2 then
		xpac = pac.ix
		ypac = pac.iy
	end

	if xghost == xpac and yghost == ypac then
		if pac.powerup.timer <= 0 then
			pac.lives = pac.lives - 1
			if pac.lives > 0 then
				restartLevel()
--				currentLevel = currentLevel-1
--				gamestate = 4
--				transitiontimer = transitiondelay
			else
				gamestate = 2
			end
			love.audio.play(deathsound)
		else
			pac.eat = true
			ghost.deathtimer = Ghosts.deathdelay
			ghost.opacity = 0
			love.audio.play(powerupsound)
		end
	end
end

function Ghosts.draw()
	for i,v in ipairs(Ghosts.list) do
		Ghosts.drawGhost(v)
	end
	love.graphics.setColor(currentcolor)
end

function Ghosts.updateGhost(dt, ghost)
	ghost.timer = ghost.timer - dt
	if ghost.timer <= 0 then
		ghost.timer = ghost.timer + Ghosts.period
		Ghosts.moveGhost(ghost)
		ghost.frameNdx = (ghost.frameNdx % 2) + 1
	end
	Ghosts.getPosition(ghost)
	Ghosts.checkdeath(ghost)		
	if ghost.deathtimer > 0 then
		ghost.deathtimer = ghost.deathtimer - dt
		if ghost.deathtimer < 0.4 then
			ghost.opacity = 1 - ( 1 - ghost.opacity ) * math.pow(0.97, 60*dt)
		end
	else
		ghost.opacity = 1 - ( 1 - ghost.opacity ) * math.pow(0.97, 60*dt)
	end

end

-- direction:
-- 1- right
-- 2- left
-- 3- up
-- 4- down

function Ghosts.moveGhost(ghost)
	local moveOptions = getMoveOptions(ghost.ix, ghost.iy)
	local dest
	if ghost.ghostType == 1 then
		-- move to player
		dest = {pac.ix, pac.iy}
	elseif ghost.ghostType == 2 then
		if harmattanDistance( {pac.ix, pac.iy}, {ghost.ix, ghost.iy} ) < 4 then
			dest = {pac.ix, pac.iy}
		else
			if pac.dir == 1 then
				dest = {pac.ix + 4, pac.iy}
			elseif pac.dir == 2 then
				dest = {pac.ix - 4, pac.iy}
			elseif pac.dir == 3 then
				dest = {pac.ix, pac.iy - 4}
			else
				dest = {pac.ix, pac.iy + 4}
			end
		end
	elseif ghost.ghostType == 3 then
		if harmattanDistance( {pac.ix, pac.iy}, {ghost.ix, ghost.iy} ) < 4 then
			dest = {pac.ix, pac.iy}
		else
			if pac.dir == 1 then
				dest = {pac.ix - 4, pac.iy}
			elseif pac.dir == 2 then
				dest = {pac.ix + 4, pac.iy}
			elseif pac.dir == 3 then
				dest = {pac.ix, pac.iy + 4}
			else
				dest = {pac.ix, pac.iy - 4}
			end
		end
	elseif ghost.ghostType == 4 then
		dest = {math.random(table.getn(maze[1])), math.random(table.getn(maze))}
	end

	for i,w in ipairs(moveOptions) do
		table.insert(w, euclidSqDistance(w,dest))
	end

	local ndx = 0
	local dist = math.pow(table.getn(maze),4) -- absurdly long
	for i,w in ipairs(moveOptions) do
		if (w[1] ~= ghost.ix_prev or w[2] ~= ghost.iy_prev) and w[3] < dist and not Ghosts.isEnemyThere(w[1],w[2]) then
				dist = w[3]
				ndx = i
		end 
	end

	if ndx > 0 then
		ghost.ix_prev = ghost.ix
		ghost.iy_prev = ghost.iy
		ghost.ix = moveOptions[ndx][1]
		ghost.iy = moveOptions[ndx][2]
	else
		local tx,ty = ghost.ix, ghost.iy
		ghost.ix = ghost.ix_prev
		ghost.iy = ghost.iy_prev
		ghost.ix_prev = tx
		ghost.iy_prev = ty
	end
end

function Ghosts.isEnemyThere(ix, iy)
	for i,v in ipairs(Ghosts.list) do
		if (v.ix == ix and v.iy == iy) or (v.ix_prev == ix and v.iy_prev == iy) then
			return true
		end
	end
	return false
end

function Ghosts.getPosition(ghost)
	-- tweening!
	ghost.x = (ghost.ix + (ghost.ix_prev-ghost.ix)*ghost.timer/Ghosts.period - 1)*cellw
	ghost.y = (ghost.iy + (ghost.iy_prev-ghost.iy)*ghost.timer/Ghosts.period - 1)*cellh
end

function getMoveOptions(ix, iy)
	local list = {}
	if ix > 1 and maze[iy][ix-1] == 0 then
		table.insert(list, {ix-1, iy})
	end
	if ix < table.getn(maze[1]) and maze[iy][ix+1] == 0 then
		table.insert(list, {ix+1, iy})
	end
	if iy > 1 and maze[iy-1][ix] == 0 then
		table.insert(list, {ix, iy-1})
	end
	if iy < table.getn(maze) and maze[iy+1][ix] == 0 then
		table.insert(list, {ix, iy+1})
	end
	return list
end



function Ghosts.drawGhost(ghost)
	if pac.powerup.timer <= 0 then
		love.graphics.setColor(currentcolor[1],currentcolor[2],currentcolor[3],ghost.opacity*255)
	else
		love.graphics.setColor(currentcolor[1]*0.7,currentcolor[2]*0.7,currentcolor[3],ghost.opacity*255)
	end
	love.graphics.draw(Ghosts.imgs[ghost.id][ghost.frameNdx], ghost.x + 20, ghost.y + 20, 0, 1, 1, 30, 30)
end


