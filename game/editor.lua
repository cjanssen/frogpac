Editor = {}

function Editor.init()
	Editor.active = false
	Editor.map = {
		width = 20,
		height = 14,
		cellW = 40,
		cellH = 40
	}
	Editor.currentCell = {-1,-1}
	Editor.mouseDown = false
	Editor.value = 0
	for i=1,Editor.map.width do
		Editor.map[i] = {}
		for j=1,Editor.map.height do
			Editor.map[i][j] =0
		end
	end
	Editor.load("currentmap.lua")
	Editor.copyMapOver()
end

function Editor.update(dt)
	if love.mouse.isDown("l") then
		local cx,cy = Editor.mapcoords(love.mouse.getX(), love.mouse.getY())
		if not Editor.mouseDown then
			Editor.mouseDown = true
			Editor.value = 1 - Editor.map[cx][cy]
		end
		if Editor.currentCell[1] ~= cx or Editor.currentCell[2] ~= cy then
			Editor.currentCell = {cx, cy}
			Editor.map[cx][cy] = Editor.value
		end
	else
		Editor.currentCell = {-1, -1}
		Editor.mouseDown = false
	end
end

function Editor.draw()
	local m = Editor.map
	love.graphics.setColor(255,0,255)
	for i=1,m.width do
		for j = 1,m.height do
			if m[i][j] == 1 then
				love.graphics.rectangle("fill", (i-1)*m.cellW, (j-1)*m.cellH, m.cellW, m.cellH)
			end
		end
	end
end

function Editor.setActive( active )
	if active then
		Editor.active = true
		Editor.load("currentmap.lua")
	else
		Editor.copyMapOver()
		Editor.save("currentmap.lua")
		Editor.active = false
	end
end

function Editor.keypressed(key)
	if key == "e" then
		Editor.setActive(false)
	end
end

function Editor.mousepressed(x,y)
	
end

function Editor.mapcoords(x,y)
	local cx = math.min(math.floor(x/Editor.map.cellW)+1, Editor.map.width)
	local cy = math.min(math.floor(y/Editor.map.cellH)+1, Editor.map.height)
	return cx, cy
end

function Editor.copyMapOver()
	maze = {}
	for i=1,Editor.map.height do
		maze[i] = {}
		for j = 1,Editor.map.width do
			maze[i][j] = Editor.map[j][i]
		end
	end
	preparewalls()
	preparedotmaze()
end

function Editor.save(filename)
	local str = Editor.serializeMap()
	love.filesystem.write(filename,str,str:len())
end

function Editor.load(filename)
	if not love.filesystem.exists(filename) then
		return
	end

	local contents,size = love.filesystem.read(filename)
	if size ~= 0 then
		Editor.parseMap(contents)
	end
end

function Editor.serializeMap()
	local str = "editormap = {\n"..
	"width = "..Editor.map.width..",\n"..
	"height = "..Editor.map.height..",\n"..
	"cellW = "..Editor.map.cellW..",\n"..
	"cellH = "..Editor.map.cellH..",\n"
	for i=1,Editor.map.width do
		str = str.."{"
		for j=1,Editor.map.height do
			str = str..Editor.map[i][j]
			if j==Editor.map.height then
				str = str.."}"
			else
				str = str..", "
			end
		end
		if i==Editor.map.width then
			str = str.."\n"
		else
			str = str..",\n"
		end
	end
	str = str.."}"
	return str
end

function Editor.parseMap(content)
	assert(loadstring(content))()
	Editor.map = editormap
end

