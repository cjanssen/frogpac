Map = {}

function Map.init()
	Map.imgblock1 = love.graphics.newImage("img/busch1.png")
	Map.imgblock2 = love.graphics.newImage("img/busch2.png")
	Map.imgblock3 = love.graphics.newImage("img/busch3.png")
	Map.imgblock4 = love.graphics.newImage("img/busch4.png")
	Map.imgblock5 = love.graphics.newImage("img/busch5.png")

	Map.imgcookie = love.graphics.newImage("img/Keks.png")
	Map.imgbanana = love.graphics.newImage("img/banana.png")
	Map.imgpear = love.graphics.newImage("img/pear.png")
	Map.imgstrawberry = love.graphics.newImage("img/strawberry.png")
	Map.imggrape = love.graphics.newImage("img/Traube.png")

	cellw, cellh = 40,40
	initLevel(1)
	if Editor then
		Editor.copyMapOver()
	end
end

function loadLevel(levelNumber)
	initLevel(levelNumber)
	startLevel()
end

function initLevel(levelNumber)
	currentLevel = levelNumber

	if levelNumber == 1 then
		maze = {
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0},
		{0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1},
		{0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1},
		{1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1},
		{0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0},
		{0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0},
		{0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0},
		{0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0},
		{0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0},
		{0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0},
		{0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
		{0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0},
		{0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0},
		{0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0} }

	elseif levelNumber == 2 then
		maze = {
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0},
		{1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
		{0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0},
		{0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0},
		{0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0},
		{0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0},
		{0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0},
		{0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0},
		{0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0},
		{0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
		{0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0},
		{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0},
		{0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		}
	elseif levelNumber == 3 then
		maze = {
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0},
		{0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
		{0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0},
		{0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0},
		{0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0},
		{0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0},
		{0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0},
		{0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
		{1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1},
		{0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0},
		{0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
		{0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0},
		{0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0},
		{0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1},
		{0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0},
		{0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0},
		{0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		}
	elseif levelNumber == 4 then
		maze = {
		{1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1},
		{0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0},
		{0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0},
		{0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0},
		{0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0},
		{0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0},
		{0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0},
		{0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0},
		{0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0},
		{0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},
		{0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1},
		{0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},
		{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1},
		{0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0},
		{0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0},
		{0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0},
		{0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
		}
	elseif levelNumber == 5 then
		maze = {
		{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0},
		{1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0},
		{1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1},
		{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1},
		{0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},
		{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
		{0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},
		{0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1},
		{1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1},
		{0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0},
		{0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0},
		{0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0},
		{0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		}
	elseif levelNumber == 6 then
		maze = {
		{0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1},
		{0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1},
		{0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0},
		{0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0},
		{1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
		{1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1},
		{1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1},
		{1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0},
		{0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0},
		{0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0},
		{0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0},
		{0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0},
		{0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1},
		{1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1},
		{1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1},
		{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0},
		{0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0},
		{0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0},
		{0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		}
	elseif levelNumber == 7 then
		maze = {
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1},
		{0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0},
		{0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0},
		{1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1},
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1},
		{0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0},
		{0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}
		}
	end

	transposemaze()
	preparewalls()
	preparedotmaze()
end

function startLevel()
	pac.score = 0
	pac.newix = 2
	pac.newiy = 3
	pac.ix = 2
	pac.iy = 3
	pac.ix_prev = 2
	pac.iy_prev = 3

	Ghosts.init()	
	pac.matchcount = 5
	pac.radius = pac.maxradius
	pac.powerup.timer = 0
end

function restartLevel()
	pac.newix = 2
	pac.newiy = 3
	pac.ix = 2
	pac.iy = 3
	pac.ix_prev = 2
	pac.iy_prev = 3

	Ghosts.init()	
	pac.matchcount = 5
	pac.radius = pac.maxradius
	pac.powerup.timer = 0
end

function transposemaze()
	local m2 = {}
	for i=1,table.getn(maze[1]) do
		m2[i] = {}
		for j = 1,table.getn(maze) do
			m2[i][j] = maze[j][i]
		end
	end
	maze = m2
end

function preparewalls()
	for i=1,table.getn(maze) do
		for j=1,table.getn(maze[1]) do
			local value = 0
			if maze[i][j] ~= 0 then
				if i>1 and maze[i-1][j] ~= 0 then
					value = value + 4
				end
				if i<table.getn(maze) and maze[i+1][j] ~= 0 then
					value = value + 8
				end
				if j>1 and maze[i][j-1] ~= 0 then
					value = value + 2
				end
				if j<table.getn(maze[i]) and maze[i][j+1] ~= 0 then
					value = value + 1
				end
				if value == 0 then value = 16 end
			end
			maze[i][j] = value
		end
	end
end


function preparedotmaze()
	local i,j
	cookiecount = 0
	dotmaze = {}
	for i=1,table.getn(maze) do
		dotmaze[i] = {}
		for j=1,table.getn(maze[i]) do
			if maze[i][j] ~= 0 then dotmaze[i][j] = 0 else dotmaze[i][j] = 1 end
		end
	end

	-- upgrades
	local pi,pj, valid
	local queue = {}
	for i=1,4 do
		valid = false
		while not valid do
			valid = true 
			pi = math.random(table.getn(maze[1]))
			pj = math.random(table.getn(maze))
			if maze[pj][pi] ~= 0 then valid = false end
			if dotmaze[pj][pi] ~= 1 then valid = false end
			for j,v in ipairs(queue) do
				if harmattanDistance({pi,pj},v) < 6 then
					valid = false
				end
			end
			if valid then
				dotmaze[pj][pi] = (i%4) + 2
				table.insert(queue,{pi,pj})
			end
		end
	end

	-- count
	for i=1,table.getn(maze) do
		for j=1,table.getn(maze[i]) do
			if dotmaze[i][j] == 1 then cookiecount = cookiecount + 1 end
		end
	end
	
end


function drawmaze()
	local i,j
	for i=1,table.getn(maze) do
		for j = 1,table.getn(maze[i]) do
			if maze[i][j] == 1 or maze[i][j] == 2 or maze[i][j] == 3 then
				-- horizontal block
				love.graphics.draw(Map.imgblock1, (j-0.5)*cellw, (i-0.5)*cellh, math.pi/2, 1, 1, 20, 20)
			elseif maze[i][j] == 4 or maze[i][j] == 8 or maze[i][j] == 12 then
				-- vertical block
				love.graphics.draw(Map.imgblock1, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif maze[i][j] == 5 then
				-- L
				love.graphics.draw(Map.imgblock2, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif maze[i][j] == 6 then
				-- J
				love.graphics.draw(Map.imgblock2, (j-0.5)*cellw, (i-0.5)*cellh, 0, -1, 1, 20, 20)
			elseif maze[i][j] == 9 then
				-- down-right
				love.graphics.draw(Map.imgblock2, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, -1, 20, 20)
			elseif maze[i][j] == 10 then
				-- down left
				love.graphics.draw(Map.imgblock2, (j-0.5)*cellw, (i-0.5)*cellh, 0, -1, -1, 20, 20)
			elseif maze[i][j] == 7 then
				-- upside-down t
				love.graphics.draw(Map.imgblock4, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif maze[i][j] == 11 then
				-- t
				love.graphics.draw(Map.imgblock4, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, -1, 20, 20)
			elseif maze[i][j] == 14 then
				-- left-looking t
				love.graphics.draw(Map.imgblock4, (j-0.5)*cellw, (i-0.5)*cellh, -math.pi/2, 1, 1, 20, 20)
			elseif maze[i][j] == 13 then
				-- right-looking t
				love.graphics.draw(Map.imgblock4, (j-0.5)*cellw, (i-0.5)*cellh, math.pi/2, 1, 1, 20, 20)
			elseif maze[i][j] == 15 then
				-- cross
				love.graphics.draw(Map.imgblock3, (j-0.5)*cellw, (i-0.5)*cellh, -math.pi/2, 1, 1, 20, 20)
			elseif maze[i][j] == 16 then
				-- dot
				love.graphics.draw(Map.imgblock5, (j-0.5)*cellw, (i-0.5)*cellh, math.pi/2, 1, 1, 20, 20)
			end
		end
	end
end

function drawdots()
	local i,j
	for i=1,table.getn(dotmaze) do
		for j = 1,table.getn(dotmaze[i]) do
			if dotmaze[i][j] == 1 then
				love.graphics.draw(Map.imgcookie, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif dotmaze[i][j] == 2 then
				love.graphics.draw(Map.imgbanana, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif dotmaze[i][j] == 3 then
				love.graphics.draw(Map.imgstrawberry, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif dotmaze[i][j] == 4 then
				love.graphics.draw(Map.imggrape, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			elseif dotmaze[i][j] == 5 then
				love.graphics.draw(Map.imgpear, (j-0.5)*cellw, (i-0.5)*cellh, 0, 1, 1, 20, 20)
			end
		end
	end
end
