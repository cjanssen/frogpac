-- todo: pac advances while keypressed
-- todo: pac advances gradually, keystrokes are "buffered" (not when going backwards)

function harmattanDistance(pos1, pos2)
	return math.abs(pos2[1]-pos1[1]) + math.abs(pos2[2]-pos1[2])
end

function euclidSqDistance(pos1, pos2)
	local dx = pos2[1] - pos1[1]
	local dy = pos2[2] - pos1[2]
	return dx*dx + dy*dy
end

function love.load()
	love.filesystem.load( "map.lua" )()
	Map.init()

	love.filesystem.load( "player.lua" )()
	pac.init()

	love.filesystem.load( "ghosts.lua" )()
	Ghosts.init()

--	love.filesystem.load("editor.lua")()
--	Editor.init()

	bgimg = love.graphics.newImage("img/bg.png")
	fonts = {
		love.graphics.newFont(24),
		love.graphics.newFont(21)
	}
	matchpic = love.graphics.newImage("img/match.png")
	matchsound = love.audio.newSource("sound/MatchStrike.ogg")
	love.graphics.setFont(love.graphics.newFont(24))

	gamestate = 0

	introimgs = {
		logo = love.graphics.newImage("img/logo.png"),
		start = love.graphics.newImage("img/buttonstart.png"),
		credits = love.graphics.newImage("img/buttoncredits.png"),
		gameover = love.graphics.newImage("img/gameover.png"),
		playagain = love.graphics.newImage("img/buttonplayagain.png")
	}
	currentoption = 1
	currentcolor = {255,255,255}

	bgsound = love.audio.newSource("sound/tm2_river007.ogg","streaming")
	bgsound:setLooping(true)
	bgsound:setVolume(0.3)

	transitiondelay = 1.5
	transitiontimer = transitiondelay

end


function love.draw()
	if Editor and Editor.active then
		love.graphics.setStencil()
		Editor.draw()
	else
		if gamestate == 0 then
			love.graphics.setColor(255,255,255)
			love.graphics.draw(introimgs.logo, 400, 200, 0, 1, 1, introimgs.logo:getWidth()/2, introimgs.logo:getHeight()/2)
			love.graphics.draw(introimgs.start, 400, 400, 0, 1, 1, introimgs.start:getWidth()/2, introimgs.start:getHeight()/2)
			love.graphics.draw(introimgs.credits, 400, 450, 0, 1, 1, introimgs.credits:getWidth()/2, introimgs.credits:getHeight()/2)
			love.graphics.print("Arrows: move  /  Space: use match  /  P : pause", 100, 550)
			if currentoption == 1 then
				love.graphics.draw(Map.imgcookie, 300, 400-20)
			else
				love.graphics.draw(Map.imgcookie, 300, 450-20)
			end
		elseif gamestate == 1 then
			local lightfraction = (pac.radius - pac.minradius) / (pac.maxradius - pac.minradius)
			local lightfractionSq = math.pow(lightfraction, 1.5)

			-- out-of-frustrum part
			currentcolor = {255*lightfractionSq,255*lightfractionSq,255*lightfractionSq}
			love.graphics.setColor(currentcolor)
			love.graphics.setStencil()
			love.graphics.draw(bgimg, 0,0)	
			drawmaze()
			drawdots()
			Ghosts.draw()
			pac.draw()

			-- visible part
			if pac.powerup.timer > 0 then
				currentcolor = {255,255,255}
			else
				currentcolor = {255,224 + (255-224)*lightfraction, 128 + (255-128)* lightfraction}
			end
			love.graphics.setColor(currentcolor)
			love.graphics.setStencil(myStencilFunction)
			love.graphics.draw(bgimg, 0,0)	
			drawmaze()
			drawdots()
			Ghosts.draw()
			pac.draw()

			-- UI
			love.graphics.setStencil()
			love.graphics.setColor(255,255,255)
			for i=1,pac.matchcount do
				love.graphics.draw(matchpic, (i+1)*60, 600-60)
			end
			for i=1,pac.lives do
				love.graphics.draw(pac.anim.frames_right[1], 800 - (i*40), 600-40, 0, 0.5, 0.5)
			end
		
			love.graphics.setFont(fonts[1])
			love.graphics.print(pac.score.."/"..cookiecount, 10, 570)
		elseif gamestate == 2 then
			love.graphics.setColor(255,255,255)
			love.graphics.draw(introimgs.logo, 400, 200, 0, 1, 1, introimgs.logo:getWidth()/2, introimgs.logo:getHeight()/2)
			love.graphics.draw(introimgs.gameover, 400, 350, 0, 1, 1, introimgs.gameover:getWidth()/2, introimgs.gameover:getHeight()/2)
			love.graphics.draw(introimgs.playagain, 400, 490, 0, 1, 1, introimgs.playagain:getWidth()/2, introimgs.playagain:getHeight()/2)
			if pac.score == cookiecount then
				love.graphics.print("YOU WIN! Score: "..totalscore, 300, 420)
			else
				love.graphics.print("You lost. Score: "..(totalscore+pac.score), 300, 420)
			end
		elseif gamestate == 3 then
			love.graphics.setColor(255,255,255)
			love.graphics.draw(introimgs.logo, 400, 200, 0, 1, 1, introimgs.logo:getWidth()/2, introimgs.logo:getHeight()/2)	
			love.graphics.print("...made at Berlin Mini Game Jam July 2013\n\ncharacter design\ngraphics\ncode\nsound", 100, 300)	
			love.graphics.print("\n\nJuliana Hüttelmeyer\nBirgit Pohl\nChristiaan Janssen\nUlysses Hsiung-Luo", 450, 300)	
		elseif gamestate == 4 then
			love.graphics.setColor(255,255,255)
			love.graphics.draw(introimgs.logo, 400, 200, 0, 1, 1, introimgs.logo:getWidth()/2, introimgs.logo:getHeight()/2)
			love.graphics.print("Level "..(currentLevel+1).." / 7", 350, 400)
		elseif gamestate == 5 then
			love.graphics.setColor(255,255,255)
			love.graphics.draw(introimgs.logo, 400, 200, 0, 1, 1, introimgs.logo:getWidth()/2, introimgs.logo:getHeight()/2)
			love.graphics.print("Game paused", 320, 400)
		end

	end
end

function love.update(dt)
	if Editor and Editor.active then
		Editor.update(dt)
	else
		if gamestate == 0 then

--			local w,h = introimgs.start:getWidth()/2, introimgs.start:getHeight()/2
--			if isPressed(400-w/2, 400-h/2, w, h) then gamestate = 1 end
		elseif gamestate == 1 then
--gamestate = 2
			pac.update(dt)
			Ghosts.update(dt)

			if pac.score == cookiecount then
				totalscore = totalscore + cookiecount
				if currentLevel == 7 then
					gamestate = 2
					currentLevel = 8
				else
					gamestate = 4
					transitiontimer = transitiondelay
				end
			end
		elseif gamestate == 2 then
		elseif gamestate == 3 then
		elseif gamestate == 4 then
			transitiontimer = transitiontimer - dt
			if transitiontimer <= 0 then
				loadLevel(currentLevel+1)
				gamestate = 1
			end
		elseif gamestate == 5 then
		end
	end
end

function isPressed(x,y,w,h)
	local mx,my = love.mouse.getX(), love.mouse.getY()
	if mx >= x and mx <= x+w and my >= y and my <= y+h and love.mouse.isDown("l") then return true end
	return false
end

function startgame()
	totalscore = 0
	gamestate = 4
	pac.lives = 5
	currentLevel = 0
	pac.score = 0
	Ghosts.init()
	pac.newix = 2
	pac.newiy = 3
	pac.ix = 2
	pac.iy = 3
	pac.ix_prev = 2
	pac.iy_prev = 3
	pac.matchcount = 5
	pac.radius = pac.maxradius
	pac.powerup.timer = 0
	love.audio.play(bgsound)
end

function love.keypressed(key)
	if key == "escape" then
		love.event.push("quit")
		return
	end

	if gamestate == 0 then
		if key == "up" or key == "down" or key == "left" or key == "right" then
			currentoption = (currentoption % 2)+1
		end
		if key == " " then
			if currentoption == 1 then
				startgame()
			elseif currentoption == 2 then
				gamestate = 3
			end
		end
--		bgsound:stop()
	elseif gamestate == 1 then
		if Editor and Editor.active then
			Editor.keypressed(key)
		else
			if Editor and key == "e" then
				Editor.setActive(true)
			end

			if key == " " then
				if pac.radius <= pac.minradius*2 and pac.matchcount > 0 then
					pac.matchcount = pac.matchcount - 1
					pac.radius = pac.maxradius
					love.audio.play(matchsound)
				end
			end

			if key == "p" then
				gamestate = 5
			end
		end

		movepac(0)
	elseif gamestate == 2 then
		if key == " " then
			gamestate = 0
		end
		bgsound:stop()
	elseif gamestate == 3 then
		if key == " " then
			gamestate = 0
		end
--		bgsound:stop()
	elseif gamestate == 5 then
		if key == "p" then
			gamestate = 1
		end
	end
end

