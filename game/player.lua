	pac = {
		x = 100,
		y = 100,
		ix = 2,
		iy = 3,
		ix_prev = 2,
		iy_prev = 3,
		newix = 2,
		newiy = 3,
		score = 0,
		dir = 1,
		candidatedir = 1,
		newdir = 1,
		radius = 800,
		minradius = 80,
		maxradius = 800,
		matchcount = 5,
		lives = 4,
		
		movetimer = 0,
		movedelay = 0.25, -- twice as fast as the ghosts
		movenow = false,
		eat = false,

		powerup = {
			timer = 0,
			delay = 8,
			blinkdelay = 2,
			blinkperiod = 0.4
		}
	}

-- direction:
-- 1- right
-- 2- left
-- 3- up
-- 4- down

function pac.init()
	pac.anim = {
		frames_right = {
			love.graphics.newImage("img/kermitseiteauf.png"),
			love.graphics.newImage("img/kermitseitezu.png")
		},
		frames_up = {
			love.graphics.newImage("img/kermitbackauf.png"),
			love.graphics.newImage("img/kermitbackzu.png")
		},
		frames_down = {
			love.graphics.newImage("img/kermitfrontauf.png"),
			love.graphics.newImage("img/kermitfrontzu.png")
		},
		frames_pow_right = {
			love.graphics.newImage("img/kermitseiteauf_power.png"),
			love.graphics.newImage("img/kermitseitezu_power.png")
		},
		frames_pow_up = {
			love.graphics.newImage("img/kermitbackauf_power.png"),
			love.graphics.newImage("img/kermitbackzu_power.png")
		},
		frames_pow_down = {
			love.graphics.newImage("img/kermitfrontauf_power.png"),
			love.graphics.newImage("img/kermitfrontzu_power.png")
		},
		frameNdx = 1,
		frameDelay = 0.3,
		frameTimer = 0
	}
	advancepac(0)
	eatcookiesound = love.audio.newSource("sound/nom_nom_short.ogg")
	eatcookiesound:setVolume(0.6)
	powerupsound = love.audio.newSource("sound/croak.ogg")
	powerdownsound = love.audio.newSource("sound/frog.ogg")
end

function pac.update(dt)
	-- move and eat
	advancepac(dt)
	eatdots(dt)

	-- light radius
	pac.radius = pac.radius * math.pow(0.996, 60*dt)
	if pac.radius < pac.minradius then
		pac.radius = pac.minradius
	end

	-- open/close mouth
	if pac.eat then
		if pac.movetimer < pac.movedelay/2 then
			pac.anim.frameNdx = 1 
		else
			pac.anim.frameNdx = 2
		end
	end

	-- powerup timer
	if pac.powerup.timer > 0 then
		pac.powerup.timer = pac.powerup.timer - dt
	end
end


function eatdots(dt)
	if dotmaze[pac.iy][pac.ix] == 1 then
		pac.eat = true
		pac.score = pac.score + 1
		dotmaze[pac.iy][pac.ix] = 0
		eatcookiesound:rewind()
		love.audio.play(eatcookiesound)
	elseif dotmaze[pac.iy][pac.ix] > 1 then
		-- powerup
		pac.eat = true
		pac.powerup.timer = pac.powerup.delay
		dotmaze[pac.iy][pac.ix] = 0
		powerupsound:rewind()
		love.audio.play(powerupsound)
	end
end

function advancepac(dt)
	pac.movetimer = pac.movetimer - dt
	if pac.movetimer <= 0 or pac.movenow then
		if pac.movenow then
			pac.movetimer = pac.movedelay - pac.movetimer
			pac.movenow = false
		else
			pac.movetimer = pac.movedelay
		end
		pac.eat = false
		pac.ix_prev = pac.ix
		pac.iy_prev = pac.iy

		-- update direction
		if pac.candidatedir == 1 and pac.ix<table.getn(maze[1]) and maze[pac.iy][pac.ix+1]==0 then
			pac.dir = 1
		elseif pac.candidatedir == 2 and pac.ix>1 and maze[pac.iy][pac.ix-1]==0 then
			pac.dir = 2
		elseif pac.candidatedir == 3 and pac.iy>1 and maze[pac.iy-1][pac.ix]==0 then
			pac.dir = 3
		elseif pac.candidatedir == 4 and pac.iy<table.getn(maze) and maze[pac.iy+1][pac.ix]==0 then
			pac.dir = 4
		end

		-- update position		
		if pac.dir == 1 and pac.ix<table.getn(maze[1]) and maze[pac.iy][pac.ix+1]==0 then
			pac.ix = pac.ix + 1
		elseif pac.dir == 2 and pac.ix>1 and maze[pac.iy][pac.ix-1]==0 then
			pac.ix = pac.ix - 1
		elseif pac.dir == 3 and pac.iy>1 and maze[pac.iy-1][pac.ix]==0 then
			pac.iy = pac.iy -1
		elseif pac.dir == 4 and pac.iy<table.getn(maze) and maze[pac.iy+1][pac.ix]==0 then
			pac.iy = pac.iy + 1
		end

		if pac.ix == pac.ix_prev and pac.iy == pac.iy_prev then
			pac.movenow = true
			pac.movetimer = 0
		end
	end

	pac.x = (pac.ix + (pac.ix_prev - pac.ix)*pac.movetimer/pac.movedelay - 1 ) *cellw
	pac.y = (pac.iy + (pac.iy_prev - pac.iy)*pac.movetimer/pac.movedelay - 1 ) *cellw
end

function movepac(dt)
	if love.keyboard.isDown("left") then
		pac.candidatedir = 2
	end
	if love.keyboard.isDown("right") then
		pac.candidatedir = 1
	end
	if love.keyboard.isDown("up") then
		pac.candidatedir = 3
	end
	if love.keyboard.isDown("down") then
		pac.candidatedir = 4
	end

	if (pac.dir == 1 and pac.candidatedir == 2) or
		(pac.dir == 2 and pac.candidatedir == 1) or
		(pac.dir == 3 and pac.candidatedir == 4) or
		(pac.dir == 4 and pac.candidatedir == 3) then
			pac.movenow = true
	end
end

function pac.draw()
	local useOriginalImage = true
	if pac.powerup.timer > 0 then
		if pac.powerup.timer > pac.powerup.blinkdelay or pac.powerup.timer%pac.powerup.blinkperiod < pac.powerup.blinkperiod/2 then
			useOriginalImage = false
		end
	end

	if useOriginalImage then
		if pac.dir == 1 then -- right
			love.graphics.draw(pac.anim.frames_right[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, 1, 1, 30, 30)
		elseif pac.dir == 2 then -- left
			love.graphics.draw(pac.anim.frames_right[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, -1, 1, 30, 30)
		elseif pac.dir == 3 then -- up
			love.graphics.draw(pac.anim.frames_up[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, 1, 1, 30, 30)
		elseif pac.dir == 4 then -- down
			love.graphics.draw(pac.anim.frames_down[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, 1, 1, 30, 30)
		end
	else
		if pac.dir == 1 then -- right
			love.graphics.draw(pac.anim.frames_pow_right[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, 1, 1, 30, 30)
		elseif pac.dir == 2 then -- left
			love.graphics.draw(pac.anim.frames_pow_right[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, -1, 1, 30, 30)
		elseif pac.dir == 3 then -- up
			love.graphics.draw(pac.anim.frames_pow_up[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, 1, 1, 30, 30)
		elseif pac.dir == 4 then -- down
			love.graphics.draw(pac.anim.frames_pow_down[pac.anim.frameNdx], pac.x + cellw/2, pac.y + cellh/2, 0, 1, 1, 30, 30)
		end
	end
end

myStencilFunction = function()
   love.graphics.circle("fill", pac.x + cellw/2, pac.y + cellh/2, pac.radius)
end

